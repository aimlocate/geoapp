# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Data(models.Model):
    bldg_name = models.TextField(db_column='BLDG_Name', blank=True, null=True)  # Field name made lowercase.
    bldg_no = models.TextField(db_column='BLDG_No', blank=True, null=True)  # Field name made lowercase.
    bldg_type = models.TextField(db_column='BLDG_Type', blank=True, null=True)  # Field name made lowercase.
    tot_flats = models.FloatField(db_column='Tot_Flats', blank=True, null=True)  # Field name made lowercase.
    rd_name = models.TextField(db_column='Rd_Name', blank=True, null=True)  # Field name made lowercase.
    block_n = models.TextField(db_column='Block_N', blank=True, null=True)  # Field name made lowercase.
    sub_loc_n = models.TextField(db_column='Sub_Loc_N', blank=True, null=True)  # Field name made lowercase.
    locality = models.TextField(db_column='Locality', blank=True, null=True)  # Field name made lowercase.
    address = models.TextField(db_column='Address', blank=True, null=True)  # Field name made lowercase.
    no_floor = models.TextField(db_column='No_Floor', blank=True, null=True)  # Field name made lowercase.
    flats_floo = models.FloatField(db_column='Flats_Floo', blank=True, null=True)  # Field name made lowercase.
    no_tower = models.FloatField(db_column='NO_Tower', blank=True, null=True)  # Field name made lowercase.
    pin_code = models.TextField(db_column='Pin_Code', blank=True, null=True)  # Field name made lowercase.
    city_name = models.TextField(db_column='City_Name', blank=True, null=True)  # Field name made lowercase.
    village_n = models.TextField(db_column='Village_N', blank=True, null=True)  # Field name made lowercase.
    tehsil_n = models.TextField(db_column='Tehsil_N', blank=True, null=True)  # Field name made lowercase.
    district_n = models.TextField(db_column='District_N', blank=True, null=True)  # Field name made lowercase.
    state_name = models.TextField(db_column='State_Name', blank=True, null=True)  # Field name made lowercase.
    remarks = models.TextField(db_column='Remarks', blank=True, null=True)  # Field name made lowercase.
    longitude = models.FloatField(db_column='Longitude', blank=True, null=True)  # Field name made lowercase.
    latitude = models.FloatField(db_column='Latitude', blank=True, null=True)  # Field name made lowercase.
    featureid = models.TextField(db_column='FeatureID', blank=True, null=True)  # Field name made lowercase.
    aim_postal = models.TextField(db_column='AIM_Postal', blank=True, null=True)  # Field name made lowercase.
    aim_addres = models.TextField(db_column='AIM_Addres', blank=True, null=True)  # Field name made lowercase.
    country = models.TextField(db_column='Country', blank=True, null=True)  # Field name made lowercase.
    admin_l1 = models.TextField(db_column='Admin_l1', blank=True, null=True)  # Field name made lowercase.
    admin_l2 = models.TextField(db_column='Admin_l2', blank=True, null=True)  # Field name made lowercase.
    poi_name = models.TextField(db_column='POI_NAME', blank=True, null=True)  # Field name made lowercase.
    geometry = models.GeometryField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'data'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class ShpShp(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=1000)
    file = models.CharField(max_length=100)
    uploaded_date = models.DateField()

    class Meta:
        managed = False
        db_table = 'shp_shp'
