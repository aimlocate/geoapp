# Use latest Ubuntu
FROM perrygeo/gdal-base:latest as production

# Update base container install
#RUN apt-get upgrade -y
ENV PYTHONUNBUFFERED=1

# This will install latest version of GDAL
RUN pip3 install GDAL==3.0.3

COPY requirements/prod ./requirements/prod
RUN pip install -r ./requirements/prod
RUN pip install gunicorn  # Install Gunicorn

COPY manage.py ./manage.py
COPY setup.cfg ./setup.cfg
COPY Makefile ./Makefile
COPY geoApp ./geoApp

EXPOSE 8000

# Assuming geoApp is the name of your project
CMD ["gunicorn", "geoApp.wsgi:application", "--bind", "0.0.0.0:8000"]

FROM production as development
COPY . .
