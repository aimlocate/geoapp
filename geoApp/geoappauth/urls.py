from django.urls import re_path as url
from django.urls import include, path
from rest_framework import routers

from geoApp.geoappauth import views
from geoApp.geoappauth.views import LocationViewSet

router = routers.DefaultRouter()
router.register(r'locations', LocationViewSet, basename='location')


urlpatterns = [
    url(r'', include('djoser.urls')),
    url(r'', include('djoser.urls.jwt')),
    path('', include(router.urls))
]