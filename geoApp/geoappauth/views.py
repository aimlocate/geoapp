from django.http import HttpResponse
from rest_framework import viewsets

from geoApp.geoappauth.models import Location
from geoApp.geoappauth.serializers import LocationSerializer
from rest_framework.response import Response


def detail(request):
    return HttpResponse("You're looking at new webiste")


# class UserLocationViewSet(viewsets.ModelViewSet):
#     queryset = UserLocation.objects.all()
#     serializer_class = UserLocationSerializer
#
#     def update(self, request, *args, **kwargs):
#         instance = self.get_object()
#         serializer = self.get_serializer(instance, data=request.data, partial=True)
#         serializer.is_valid(raise_exception=True)
#         serializer.save()
#         return Response(serializer.data)
#
#     def list(self, request, *args, **kwargs):
#         email = request.query_params.get("email", None)
#         if email is not None:
#             queryset = self.queryset.filter(user__email=email)
#         else:
#             queryset = self.queryset.all()
#         serializer = self.get_serializer(queryset, many=True)
#         return Response(serializer.data)


class LocationViewSet(viewsets.ModelViewSet):
    serializer_class = LocationSerializer
    queryset = Location.objects.all()
