from django.contrib import admin

# Register your models here.
from geoApp.geoappauth.models import Location, CustomUser

admin.site.register(Location)
admin.site.register(CustomUser)
