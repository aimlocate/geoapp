from django.urls import include, path
from rest_framework import routers
from . import views
from .views import DataRadiusViewSet, DataViewSet

router = routers.DefaultRouter()
router.register(r'data', views.DataViewSet, basename='data')
# router.register(r'withinradius', DataRadiusViewSet, basename='dataradius')
router.register(r'locations/(?P<location_name>\w+)/data', DataViewSet, basename='data')
router.register(r'locations/(?P<location_name>\w+)/radius', DataRadiusViewSet, basename='dataradius')

urlpatterns = [
    path('', include(router.urls)),
    path('locations/<str:location_name>/data/<int:pk>/', views.DataViewSet.as_view({'put': 'update', 'patch': 'update'}))
]