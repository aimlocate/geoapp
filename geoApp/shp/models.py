import datetime
import glob
import os
import zipfile

from django.db import models
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
import geopandas as gpd
# Create your models here.
from django.utils import timezone
from django.utils.timezone import now
from geo.Geoserver import Geoserver
from django.contrib.gis.db import models
from sqlalchemy import *

from geoApp.geoappauth.models import CustomUser, Location


class Shp(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=1000, blank=True)
    file = models.FileField(upload_to='%Y/%m/%d')
    uploaded_date = models.DateField(default=datetime.date.today, blank=True)

    def __str__(self):
        return self.name


@receiver(post_save, sender=Shp)
def publish_data(sender, instance, created, **kwargs):
    file = instance.file.path
    file_format = os.path.basename(file).split('.')[-1]
    file_name = os.path.basename(file).split('.')[0]
    file_path = os.path.dirname(file)
    name = instance.name
    conn_str = 'postgresql://app_user:app_user12345@3.141.172.115:5432/aimlocate'
    # conn_str = 'postgresql://postgres:admin@localhost:5432/postgres'
    # geo = Geoserver('http://127.0.0.1:8080/geoserver',
    #                 username='admin', password='geoserver')

    with zipfile.ZipFile(file, 'r') as zip_ref:
        zip_ref.extractall(file_path)

    os.remove(file)  # remove zip file

    shp = glob.glob(r'{}/**/*.shp'.format(file_path),
                    recursive=True)  # to get shp

    try:
        req_shp = shp[0]
        gdf = gpd.read_file(req_shp)  # make geodataframe
        gdf['location_id'] = 1
        eng = create_engine(conn_str)
        gdf.to_postgis(con=eng, schema='public', name=name, if_exists="append")
        for s in shp:
            os.remove(s)
    except Exception as e:
        for s in shp:
            os.remove(s)
        instance.delete()
        print("There is problem during shp upload: ", e)

    # geo.create_featurestore(store_name='geoApp', workspace='aimlocate', db='aimlocate',
    #                         host='localhost', pg_user='postgres', pg_password='admin', schema='public')
    # print('coming here*************************************')
    # geo.publish_featurestore(
    #     workspace='aimlocate', store_name='geoApp', pg_table=name)


class Data(models.Model):
    id = models.AutoField(primary_key=True)
    bldg_name = models.TextField(db_column='BLDG_Name', blank=True, null=True)  # Field name made lowercase.
    bldg_no = models.TextField(db_column='BLDG_No', blank=True, null=True)  # Field name made lowercase.
    bldg_type = models.TextField(db_column='BLDG_Type', blank=True, null=True)  # Field name made lowercase.
    tot_flats = models.FloatField(db_column='Tot_Flats', blank=True, null=True)  # Field name made lowercase.
    rd_name = models.TextField(db_column='Rd_Name', blank=True, null=True)  # Field name made lowercase.
    block_n = models.TextField(db_column='Block_N', blank=True, null=True)  # Field name made lowercase.
    sub_loc_n = models.TextField(db_column='Sub_Loc_N', blank=True, null=True)  # Field name made lowercase.
    locality = models.TextField(db_column='Locality', blank=True, null=True)  # Field name made lowercase.
    address = models.TextField(db_column='Address', blank=True, null=True)  # Field name made lowercase.
    no_floor = models.TextField(db_column='No_Floor', blank=True, null=True)  # Field name made lowercase.
    flats_floo = models.FloatField(db_column='Flats_Floo', blank=True, null=True)  # Field name made lowercase.
    no_tower = models.FloatField(db_column='NO_Tower', blank=True, null=True)  # Field name made lowercase.
    pin_code = models.TextField(db_column='Pin_Code', blank=True, null=True)  # Field name made lowercase.
    city_name = models.TextField(db_column='City_Name', blank=True, null=True)  # Field name made lowercase.
    village_n = models.TextField(db_column='Village_N', blank=True, null=True)  # Field name made lowercase.
    tehsil_n = models.TextField(db_column='Tehsil_N', blank=True, null=True)  # Field name made lowercase.
    district_n = models.TextField(db_column='District_N', blank=True, null=True)  # Field name made lowercase.
    state_name = models.TextField(db_column='State_Name', blank=True, null=True)  # Field name made lowercase.
    remarks = models.TextField(db_column='Remarks', blank=True, null=True)  # Field name made lowercase.
    longitude = models.FloatField(db_column='Longitude', blank=True, null=True)  # Field name made lowercase.
    latitude = models.FloatField(db_column='Latitude', blank=True, null=True)  # Field name made lowercase.
    featureid = models.TextField(db_column='FeatureID', blank=True, null=True)  # Field name made lowercase.
    aim_postal = models.TextField(db_column='AIM_Postal', blank=True, null=True)  # Field name made lowercase.
    aim_addres = models.TextField(db_column='AIM_Addres', blank=True, null=True)  # Field name made lowercase.
    country = models.TextField(db_column='Country', blank=True, null=True)  # Field name made lowercase.
    admin_l1 = models.TextField(db_column='Admin_l1', blank=True, null=True)  # Field name made lowercase.
    admin_l2 = models.TextField(db_column='Admin_l2', blank=True, null=True)  # Field name made lowercase.
    poi_name = models.TextField(db_column='POI_NAME', blank=True, null=True)  # Field name made lowercase.
    created_by = models.ForeignKey(CustomUser, null=True, blank=True, on_delete=models.CASCADE, default=1)
    created_at = models.DateTimeField(null=True, blank=True, default=timezone.now)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    geometry = models.GeometryField(blank=True, null=True)

    class Meta:
        abstract = True


class Chennai(Data):
    class Meta:
        db_table = 'chennai'


class Navajo(Data):
    class Meta:
        db_table = 'navajo'


class Hq(Data):
    class Meta:
        db_table = 'hq'


class Srilanka(Data):
    class Meta:
        db_table = 'srilanka'
