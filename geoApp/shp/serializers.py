from rest_framework_gis.serializers import GeoFeatureModelSerializer
from rest_framework import serializers
from .models import Shp, Data, Chennai, Navajo, Hq, Srilanka


class DataSerializer(GeoFeatureModelSerializer):
    class Meta:
        model = Data
        fields = '__all__'
        geo_field = 'geometry'
        extra_fields = ['created_by']
        abstract = True


class ChennaiSerializer(DataSerializer):
    class Meta(DataSerializer.Meta):
        model = Chennai


class NavajoSerializer(DataSerializer):
    class Meta(DataSerializer.Meta):
        model = Navajo


class HqSerializer(DataSerializer):
    class Meta(DataSerializer.Meta):
        model = Hq


class SrilankaSerializer(DataSerializer):
    class Meta(DataSerializer.Meta):
        model = Srilanka
