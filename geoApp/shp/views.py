from django.contrib.gis.measure import Distance, D
from rest_framework.response import Response
from rest_framework import viewsets, status
from rest_framework.status import HTTP_200_OK

# Create your views here.
from .models import Data, Chennai, Navajo, Hq, Srilanka
from geoApp.shp.serializers import DataSerializer, ChennaiSerializer, NavajoSerializer, HqSerializer, SrilankaSerializer
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import Distance

from ..geoappauth.models import Location


class DataViewSet(viewsets.ModelViewSet):
    # queryset = Data.objects.none()  # This will be overridden in get_queryset()

    def get_queryset(self):
        location_name = self.kwargs['location_name']
        location = Location.objects.get(name=location_name)

        if location.name == "chennai":
            return Chennai.objects.all()
        if location.name == "navajo":
            return Navajo.objects.all()
        if location.name == "hq":
            return Hq.objects.all()
        if location.name == "srilanka":
            return Srilanka.objects.all()
        # More conditions for each DataLocation model ..

    def get_serializer_class(self):
        location_name = self.kwargs['location_name']
        location = Location.objects.get(name=location_name)

        if location.name == "chennai":
            return ChennaiSerializer
        if location.name == "navajo":
            return NavajoSerializer
        if location.name == "hq":
            return HqSerializer
        if location.name == "srilanka":
            return SrilankaSerializer

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)


class DataRadiusViewSet(viewsets.ModelViewSet):
    serializer_class = DataSerializer

    def list(self, request, *args, **kwargs):
        serializer = None
        latitude = float(request.query_params.get('latitude'))
        longitude = float(request.query_params.get('longitude'))
        radius = float(request.query_params.get('radius'))
        location_name = self.kwargs['location_name']
        location = Location.objects.get(name=location_name)

        center_point = Point(longitude, latitude)  # Create a Point object from latitude and longitude

        if location.name == "chennai":
            queryset = Chennai.objects.filter(geometry__distance_lte=(center_point, D(mi=radius)))
            serializer = ChennaiSerializer(queryset, many=True)
        if location.name == "navajo":
            queryset = Navajo.objects.filter(geometry__distance_lte=(center_point, D(mi=radius)))
            serializer = NavajoSerializer(queryset, many=True)
        if location.name == "hq":
            queryset = Hq.objects.filter(geometry__distance_lte=(center_point, D(mi=radius)))
            serializer = HqSerializer(queryset, many=True)
        if location.name == "srilanka":
            queryset = Srilanka.objects.filter(geometry__distance_lte=(center_point, D(mi=radius)))
            serializer = SrilankaSerializer(queryset, many=True)
        # More conditions for each DataLocation model and serializer...

        return Response(serializer.data, status=status.HTTP_200_OK)

    def get_serializer_class(self):
        location_name = self.kwargs['location_name']
        location = Location.objects.get(name=location_name)

        if location.name == "chennai":
            return ChennaiSerializer
        if location.name == "navajo":
            return NavajoSerializer
        if location.name == "hq":
            return HqSerializer
        if location.name == "srilanka":
            return SrilankaSerializer
