tag=latest
organization=sujitk3
image=geoapp
tag=v0.8.0

build:
	docker build --force-rm $(options) -t geoapp:latest .

build-prod:
	$(MAKE) build options="--target production"

push:
	docker tag $(image):latest $(organization)/$(image):$(tag)
	docker push $(organization)/$(image):$(tag)

compose-start:
	docker-compose up --remove-orphans $(options)

compose-stop:
	docker-compose down --remove-orphans $(options)

compose-manage-py:
	docker-compose run --rm $(options) website python manage.py $(cmd)

start-server:
	python manage.py runserver 0.0.0.0:8000

migrate:
	python manage.py migrate

helm-deploy:
	helm upgrade --install geoapp ./helm/geoapp
